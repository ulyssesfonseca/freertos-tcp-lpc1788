################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../FreeRTOS-Plus-TCP/FreeRTOS_ARP.c \
../FreeRTOS-Plus-TCP/FreeRTOS_DHCP.c \
../FreeRTOS-Plus-TCP/FreeRTOS_DNS.c \
../FreeRTOS-Plus-TCP/FreeRTOS_IP.c \
../FreeRTOS-Plus-TCP/FreeRTOS_Sockets.c \
../FreeRTOS-Plus-TCP/FreeRTOS_Stream_Buffer.c \
../FreeRTOS-Plus-TCP/FreeRTOS_TCP_IP.c \
../FreeRTOS-Plus-TCP/FreeRTOS_TCP_WIN.c \
../FreeRTOS-Plus-TCP/FreeRTOS_UDP_IP.c 

OBJS += \
./FreeRTOS-Plus-TCP/FreeRTOS_ARP.o \
./FreeRTOS-Plus-TCP/FreeRTOS_DHCP.o \
./FreeRTOS-Plus-TCP/FreeRTOS_DNS.o \
./FreeRTOS-Plus-TCP/FreeRTOS_IP.o \
./FreeRTOS-Plus-TCP/FreeRTOS_Sockets.o \
./FreeRTOS-Plus-TCP/FreeRTOS_Stream_Buffer.o \
./FreeRTOS-Plus-TCP/FreeRTOS_TCP_IP.o \
./FreeRTOS-Plus-TCP/FreeRTOS_TCP_WIN.o \
./FreeRTOS-Plus-TCP/FreeRTOS_UDP_IP.o 

C_DEPS += \
./FreeRTOS-Plus-TCP/FreeRTOS_ARP.d \
./FreeRTOS-Plus-TCP/FreeRTOS_DHCP.d \
./FreeRTOS-Plus-TCP/FreeRTOS_DNS.d \
./FreeRTOS-Plus-TCP/FreeRTOS_IP.d \
./FreeRTOS-Plus-TCP/FreeRTOS_Sockets.d \
./FreeRTOS-Plus-TCP/FreeRTOS_Stream_Buffer.d \
./FreeRTOS-Plus-TCP/FreeRTOS_TCP_IP.d \
./FreeRTOS-Plus-TCP/FreeRTOS_TCP_WIN.d \
./FreeRTOS-Plus-TCP/FreeRTOS_UDP_IP.d 


# Each subdirectory must supply rules for building sources it contributes
FreeRTOS-Plus-TCP/%.o: ../FreeRTOS-Plus-TCP/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -DCORE_M3 -D__LPC177X_8X__ -DUSE_SD -DUSE_USB -D__USE_LPCOPEN -D__CODE_RED -DUSB_CAN_BE_HOST -DUSB_HOST_ONLY -DSVNREV='"$(shell svn info --show-item=revision E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -Dold_SVN_REV='"$(shell svnversion -n E:\PROJETOS\CLIENTES\PRATICA\IHM5_POL\SOF)"' -DSVN_REV='"$(shell svn info -r HEAD --show-item revision)"' -D__REDLIB__ -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\FreeRTOS-TCP_LPC1788\emwin\GUI" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\FreeRTOS-TCP_LPC1788\emwin\Inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\FreeRTOS-TCP_LPC1788\emwin\Config" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\FreeRTOS-TCP_LPC1788\emwin\System\HW" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\FreeRTOS-TCP_LPC1788\app\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\FreeRTOS-TCP_LPC1788\board\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\FreeRTOS-TCP_LPC1788\mcu\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\FreeRTOS-TCP_LPC1788\gui_emb\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\FreeRTOS-TCP_LPC1788\utilities\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\FreeRTOS-TCP_LPC1788\mcp9803\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\FreeRTOS-TCP_LPC1788\lpcusblib\Drivers\USB" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\FreeRTOS-TCP_LPC1788\fatfs\inc" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\FreeRTOS-TCP_LPC1788\FreeRTOS-Plus-TCP\include" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\FreeRTOS-TCP_LPC1788\freertos\include" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\FreeRTOS-TCP_LPC1788\freertos\portable\gcc\ARM_CM3" -I"E:\PROJETOS\CLIENTES\PRATICA\ETHERNET_TESTE_FREERTOS-TCP\FreeRTOS-TCP_LPC1788\FreeRTOS-Plus-TCP\portable\Compiler\GCC" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -fstack-usage -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


