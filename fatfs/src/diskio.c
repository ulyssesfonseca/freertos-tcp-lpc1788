/*-----------------------------------------------------------------------*/
/* Low level disk I/O module 									         */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include <string.h>
#include "board.h"
#include "chip.h"
#include "diskio.h"

/* buffer size (in byte) for R/W operations */
#define BUFFER_SIZE     4096

#if USE_SD
#include "fsmci_cfg.h"
#include "sdControl.h"
static CARD_HANDLE_T *SD_hCard;
#endif

#if USE_USB
#include "fsusb_cfg.h"
static DISK_HANDLE_T *USB_hDisk;
SCSI_Capacity_t DiskCapacity;
#endif


static volatile DSTATUS SD_Status = STA_NOINIT;
static volatile DSTATUS USB_Status = STA_NOINIT;

//========================== ROUTINE ACCESS DRIVERS =============================

/*-----------------------------------------------------------------------*/
/* Initialize Disk Drive                                                  */
/*-----------------------------------------------------------------------*/
DSTATUS disk_initialize (BYTE drv){

	static uint8_t _rtc_init = 0;

	switch(drv){
		case SD:{
			SD_Status = STA_NOINIT;
#if USE_SD

			if (SD_Status != STA_NOINIT) {
				return SD_Status;					/* card is already enumerated */
			}

#if !_FS_READONLY
			if (_rtc_init==0) {
				FSMCI_InitRealTimeClock();
				_rtc_init=1;
			}
#endif

			/* Initialize the Card Data Strucutre */
			SD_hCard = FSMCI_CardInit();

			/* Reset */
			SD_Status = STA_NOINIT;

			FSMCI_CardInsertWait(SD_hCard); /* Wait for card to be inserted */

			/* Enumerate the card once detected. Note this function may block for a little while. */
			if (!FSMCI_CardAcquire(SD_hCard)) {
				DEBUGOUT("Card Acquire failed...\r\n");
				return SD_Status;
			}

			SD_Status &= ~STA_NOINIT;
#endif
			return SD_Status;
		}break;

		case USB:{
			USB_Status = STA_NOINIT;
#if USE_USB

			/*	if (Stat & STA_NODISK) return Stat;	*//* No card in the socket */
			//DEBUGOUT("disk_initialize\n");
			if (USB_Status != STA_NOINIT) {
				return USB_Status;					/* card is already enumerated */
			}

#if !_FS_READONLY
			if (_rtc_init==0) {
				FSUSB_InitRealTimeClock();
				_rtc_init=1;
			}
#endif
			/* Initialize the Card Data Strucutre */
			USB_hDisk = FSUSB_DiskInit();
			/* Reset */
			USB_Status = STA_NOINIT;
			FSUSB_DiskInsertWait(USB_hDisk); /* Wait for card to be inserted */
			/* Enumerate the card once detected. Note this function may block for a little while. */
			if (!FSUSB_DiskAcquire(USB_hDisk)) {
				DEBUGOUT("Disk Enumeration failed...\r\n");
				return USB_Status;
			}
			DEBUGOUT("disk_initialize end\n");
			USB_Status &= ~STA_NOINIT;
#endif
			return USB_Status;
		}break;

		default:{
			return STA_NOINIT;
		}
	}

}

/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/
DRESULT disk_ioctl (BYTE drv, BYTE ctrl, void *buff){
	DRESULT res;
	BYTE *ptr = buff;


	switch(drv){
		case SD:{
			res = RES_ERROR;
#if USE_SD
			if (SD_Status & STA_NOINIT) {
				return RES_NOTRDY;
			}
			switch (ctrl) {
				case CTRL_SYNC:	/* Make sure that no pending write process */
					if (FSMCI_CardReadyWait(SD_hCard, 50)) {
						res = RES_OK;
					}
					break;

				case GET_SECTOR_COUNT:	/* Get number of sectors on the disk (DWORD) */
					*(DWORD *) buff = FSMCI_CardGetSectorCnt(SD_hCard);
					res = RES_OK;
					break;

				case GET_SECTOR_SIZE:	/* Get R/W sector size (WORD) */
					*(WORD *) buff = FSMCI_CardGetSectorSz(SD_hCard);
					res = RES_OK;
					break;

				case GET_BLOCK_SIZE:/* Get erase block size in unit of sector (DWORD) */
					*(DWORD *) buff = FSMCI_CardGetBlockSz(SD_hCard);
					res = RES_OK;
					break;

				case MMC_GET_TYPE:		/* Get card type flags (1 byte) */
					*ptr = FSMCI_CardGetType(SD_hCard);
					res = RES_OK;
					break;

				case MMC_GET_CSD:		/* Receive CSD as a data block (16 bytes) */
					*((uint32_t *) buff + 0) = FSMCI_CardGetCSD(SD_hCard, 0);
					*((uint32_t *) buff + 1) = FSMCI_CardGetCSD(SD_hCard, 1);
					*((uint32_t *) buff + 2) = FSMCI_CardGetCSD(SD_hCard, 2);
					*((uint32_t *) buff + 3) = FSMCI_CardGetCSD(SD_hCard, 3);
					res = RES_OK;
					break;

				case MMC_GET_CID:		/* Receive CID as a data block (16 bytes) */
					*((uint32_t *) buff + 0) = FSMCI_CardGetCID(SD_hCard, 0);
					*((uint32_t *) buff + 1) = FSMCI_CardGetCID(SD_hCard, 1);
					*((uint32_t *) buff + 2) = FSMCI_CardGetCID(SD_hCard, 2);
					*((uint32_t *) buff + 3) = FSMCI_CardGetCID(SD_hCard, 3);
					res = RES_OK;
					break;

				case MMC_GET_SDSTAT:/* Receive SD status as a data block (64 bytes) */
					if (FSMCI_CardGetState(SD_hCard, (uint8_t *) buff)) {
						res = RES_OK;
					}
					break;
				default:
					res = RES_PARERR;
					break;
			}
			return res;
#endif
		}break;
		case USB:{
			res = RES_ERROR;
#if USE_USB
			if (USB_Status & STA_NOINIT) {
				return RES_NOTRDY;
			}

			switch (ctrl) {
				case CTRL_SYNC:	/* Make sure that no pending write process */
					if (FSUSB_DiskReadyWait(USB_hDisk, 50)) {
						res = RES_OK;
					}
					break;

				case GET_SECTOR_COUNT:	/* Get number of sectors on the disk (DWORD) */
					*(DWORD *) buff = FSUSB_DiskGetSectorCnt(USB_hDisk);
					res = RES_OK;
					break;

				case GET_SECTOR_SIZE:	/* Get R/W sector size (WORD) */
					*(WORD *) buff = FSUSB_DiskGetSectorSz(USB_hDisk);
					res = RES_OK;
					break;

				case GET_BLOCK_SIZE:/* Get erase block size in unit of sector (DWORD) */
					*(DWORD *) buff = FSUSB_DiskGetBlockSz(USB_hDisk);
					res = RES_OK;
					break;

				default:
					res = RES_PARERR;
					break;
			}
			return res;
#endif
		}break;
		default:{
			return RES_PARERR;
		}
	}
}

/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/
DRESULT disk_read (BYTE drv, BYTE *buff, DWORD sector, UINT count){

	switch (drv) {
		case SD: {
#if USE_SD
			if (!count) {
				return RES_PARERR;
			}
			if (SD_Status & STA_NOINIT) {
				return RES_NOTRDY;
			}

			if (FSMCI_CardReadSectors(SD_hCard, buff, sector, count)) {
				return RES_OK;
			}
#endif
			return RES_ERROR;
		}break;
		case USB:{
#if USE_USB
			if (!count) {
				return RES_PARERR;
			}
			if (USB_Status & STA_NOINIT) {
				return RES_NOTRDY;
			}

			if (FSUSB_DiskReadSectors(USB_hDisk, buff, sector, count)) {
				return RES_OK;
			}
#endif
			return RES_ERROR;

		}break;
		default:{
			return RES_PARERR;
		}
	}
}

/*-----------------------------------------------------------------------*/
/* Get Disk Status                                                       */
/*-----------------------------------------------------------------------*/
DSTATUS disk_status (BYTE drv){

	switch (drv) {
		case SD: {
			return SD_Status;
		}break;

		case USB: {
			return USB_Status;
		}break;

		default: {
			return STA_NOINIT;
		}
	}
}

/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/
#if _READONLY == 0
DRESULT disk_write (BYTE drv, const BYTE *buff, DWORD sector, UINT count){

	if (!count) return RES_PARERR;
	switch (drv) {
		case SD: {
#if USE_SD
			if (!count) {
				return RES_PARERR;
			}
			if (SD_Status & STA_NOINIT) {
				return RES_NOTRDY;
			}

			if ( FSMCI_CardWriteSectors(SD_hCard, (void *) buff, sector, count)) {
				return RES_OK;
			}
#endif
			return RES_ERROR;

		}break;

		case USB:{
#if USE_USB
			if (!count) {
				return RES_PARERR;
			}
			if (USB_Status & STA_NOINIT) {
				return RES_NOTRDY;
			}

			if (FSUSB_DiskWriteSectors(USB_hDisk, (void *) buff, sector, count)) {
				return RES_OK;
			}
#endif
			return RES_ERROR;

		}break;
		default:{
			return RES_PARERR;
		}
	}

}
#endif /* _READONLY == 0 */
