/*
 * App.h
 *
 *  Created on: 26 de jan de 2017
 *      Author: Ulysses
 */

#ifndef INC_APP_H_
#define INC_APP_H_


#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "GUI.h"
#include "DIALOG.h"
#include "GRAPH.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "ff.h"

#include "MCP9803.h"
#include "GUI_EMB.h"


#include "Textos.h"


#include "chip.h"
#include "board_types.h"
#include "rtc.h"

#include "diskio.h"


#ifdef DEBUG
#	define  Debug_Log(...)       //UART_Printf(__VA_ARGS__)
#else
#	define  Debug_Log(...)
#endif


/* Define a name that will be used for LLMNR and NBNS searches. */
#define mainHOST_NAME				"RTOSDemo"
#define mainDEVICE_NICK_NAME		"windows_demo"



extern PIN_COM_TypeDef LED_MOD[2];
extern PIN_COM_TypeDef LCD [21];

volatile uint8_t _f_STATUS = 0;
volatile uint8_t _f_SD_Connet = 0;
volatile uint8_t _f_WatchDogFeed = 0;

volatile uint8_t _c_5seconds = 0;

volatile uint16_t _c_tsk_Gui = 0;
volatile uint16_t _c_tsk_Led01 = 0;
volatile uint16_t _c_tsk_Ms = 0;
volatile uint16_t _c_tsk_IOT = 0;

uint32_t timerCntms;



#endif /* INC_APP_H_ */


