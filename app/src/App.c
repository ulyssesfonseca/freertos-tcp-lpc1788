/*
 * App.c
 *
 *  Created on: 26 de jan de 2017
 *      Author: Ulysses
 */

//https://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_TCP/index.html
//https://github.com/YoungYoung619/Stm32-FreeRTOS-TCPIP/blob/master/User/netInfoConfig.c
//https://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_TCP/TCP_Networking_Tutorial_Sending_TCP_Data.html
#include "App.h"

#include "board.h"

#include "projdefs.h"
#include "FreeRTOS_Sockets.h"
#include "FreeRTOS_IP.h"


#include <stdarg.h>
#include <stdio.h>
#include <string.h>


static void task_MainGUI( void *pvParameters );
static void task_USB( void *pvParameters );
static void task_Led01( void *pvParameters );
static void task_IOT(void *pvParameters);
static void task_Ms( void *pvParameters );

TaskHandle_t hdl_IOT;

static __attribute__ ((used,section(".TEMP_APP"))) uint8_t heap_sram_lower[50*1024];
static HeapRegion_t xHeapRegions[] =
{
    {
        &heap_sram_lower[0], sizeof(heap_sram_lower)
    },
    {
        NULL, 0 // << Terminates the array.
    }
};




void GPIO_Set(uint8_t pin, uint8_t state){
	if(!state){
		LPC_GPIO2->CLR |= (1<<pin);
	}
	else{
		LPC_GPIO2->SET |= (1<<pin);
	}
}

void Buz_Set(uint8_t pState){

}

uint64_t sd_free_space_bytes(void){
	DWORD fre_clust;
	uint8_t res=0;


	/* Get volume information and free clusters of drive 1 */
	res = f_getfree("0:", &fre_clust, &fs_sd);
	if (res){return 0;}

	return (uint64_t)((fs_sd->free_clst) * (fs_sd->csize));

}

void vApplicationTickHook( void ) { }

void vApplicationIdleHook( void ){}

void vApplicationMallocFailedHook( void ) {}
//void vConfigureTimerForRunTimeStats( void ) { }

void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName )
{
	/* This function will get called if a task overflows its stack. */

	( void ) pxTask;
	( void ) pcTaskName;

	for( ;; );
}



//----------------------------------------------------------------------------
//	RELOGIO E RTC 1SEG
//----------------------------------------------------------------------------
void set_rtctime(void){
	RTC rtc;

//	rtc.sec		= 0;
//	rtc.min		= DATA_HORA.min;
//	rtc.hour	= DATA_HORA.hora;
//	rtc.mday	= DATA_HORA.dia;
//	rtc.month	= DATA_HORA.mes;
//	rtc.year	= DATA_HORA.ano;
//
//	rtc_settime(&rtc);
}
void get_rtctime(void){
	RTC rtc;
	rtc_gettime(&rtc);

//	DATA_HORA.seg = rtc.sec;
//	DATA_HORA.min = rtc.min;
//	DATA_HORA.hora= rtc.hour;
//	DATA_HORA.dia = rtc.mday;
//	DATA_HORA.mes = rtc.month;
//	DATA_HORA.ano = rtc.year;
//	DATA_HORA.week= week_day(DATA_HORA.dia, DATA_HORA.mes, DATA_HORA.ano);
}
void RTC_IRQHandler(void){

	if (Chip_RTC_GetIntPending(LPC_RTC, RTC_INT_COUNTER_INCREASE)){

		/* Exec. every second
		 **/

		if(_c_5seconds>5){
			DEBUGOUT("GUI:%d Led:%d Ms:%d IOT:%d %d\n",_c_tsk_Gui,_c_tsk_Led01,_c_tsk_Ms,_c_tsk_IOT,timerCntms);
			_c_5seconds = 0;
		}
		_c_tsk_Gui=_c_tsk_Led01=_c_tsk_Ms=_c_tsk_IOT = 0;

		_c_5seconds++;

		Chip_RTC_ClearIntPending(LPC_RTC, RTC_INT_COUNTER_INCREASE);
	}
}



/* PRINT OF SCREENS OF PROJECT
 */
void _print_Tela_Em_Desenvolvimento(void){

	if(ScrLoad){
		//Draw_Background();

		_DrawButtonTransparente(51,100,750,300,TXT_DESENVOLVIMENTO[ID_IDIOMA],GUI_WHITE,&GUI_Font20_ASCII,CENTER,CENTER);
	}

	ScrLoad=0; UpdateScr=0;
}


void WatchDog_Init(void){
#ifdef WATCHDOG

	//Configurações do Watchdog
//	WWDT_SetTimerConstant(500000);  //(4 x 500.000)/500.000 = 4 seg
//	WWDT_SetMode(WWDT_RESET_MODE,ENABLE);
//	WWDT_Enable(ENABLE);
//	WWDT_FeedStdSeq();
//	if (WWDT_GetStatus(WWDT_TIMEOUT_FLAG)) {_f_WatchDog=1;}
//	WWDT_ClrTimeOutFlag();

	Chip_WWDT_Init(LPC_WWDT);
	Chip_WWDT_SetTimeOut(LPC_WWDT, WDT_OSC*4);

	Chip_WWDT_SetOption(LPC_WWDT, WWDT_WDMOD_WDRESET);

	NVIC_ClearPendingIRQ(WDT_IRQn);
	NVIC_EnableIRQ(WDT_IRQn);

	Chip_WWDT_Start(LPC_WWDT);

	if(Chip_WWDT_GetStatus(LPC_WWDT)&WWDT_WDMOD_WDRESET){_f_WatchDog=1;}
	Chip_WWDT_ClearStatusFlag(LPC_WWDT, WWDT_WDMOD_WDTOF | WWDT_WDMOD_WDINT);

	_f_WatchDogFeed=0x00;
#endif
}
//----------------------------------------------------------------------------
//	BOARD INIT
//----------------------------------------------------------------------------

void BoardInit(void){

	Chip_IOCON_PinMuxSet(LPC_IOCON, LED_MOD[0].PORT, LED_MOD[0].PIN, LED_MOD[0].FUNC);
	Chip_IOCON_PinMuxSet(LPC_IOCON, LED_MOD[1].PORT, LED_MOD[1].PIN, LED_MOD[1].FUNC);

	LPC_GPIO2->DIR |= (1<<26);
	LPC_GPIO2->SET |= (1<<26);

	Chip_RTC_Init(LPC_RTC);
	Chip_RTC_CntIncrIntConfig (LPC_RTC, RTC_AMR_CIIR_IMSEC, ENABLE);
	Chip_RTC_ClearIntPending(LPC_RTC, RTC_INT_COUNTER_INCREASE);
	NVIC_EnableIRQ(RTC_IRQn);
	Chip_RTC_Enable(LPC_RTC, ENABLE);


	Chip_EEPROM_Init(LPC_EEPROM);

	__low_level_init();


	Board_I2C_Init(I2C0);

	/* Initialize I2C */
	Chip_I2C_Init(I2C0);
	Chip_I2C_SetClockRate(I2C0, 400000);

	Chip_I2C_SetMasterEventHandler(I2C0, Chip_I2C_EventHandlerPolling);


	WatchDog_Init();

	Init_SDMMC();

}

void msDelay(uint32_t ms)
{
	vTaskDelay((configTICK_RATE_HZ * ms) / 1000);
}

//----------------------------------------------------------------------------
//	TASKS
//----------------------------------------------------------------------------
void MainTask(void);

uint8_t ucMACAddress[6] = {0x00, 0x1A, 0xF1, 0x01, 0x84, 0x0E};
static const uint8_t ucIPAddress[ 4 ] = { 192, 168, 222, 201 };
static const uint8_t ucNetMask[ 4 ] = { 255, 255, 255, 0 };
static const uint8_t ucGatewayAddress[ 4 ] = { 192, 168, 222, 1 };
static const uint8_t ucDNSServerAddress[ 4 ] = { 192, 168, 222, 1 };

void vApplicationIPNetworkEventHook( eIPCallbackEvent_t eNetworkEvent ){
	uint32_t ulIPAddress, ulNetMask, ulGatewayAddress, ulDNSServerAddress;
	static BaseType_t xTasksAlreadyCreated = pdFALSE;
	int8_t cBuffer[ 16 ];

	DEBUGOUT("vApplicationIPNetworkEventHook\n");
    /* Both eNetworkUp and eNetworkDown events can be processed here. */
    if( eNetworkEvent == eNetworkUp ){
        /* Create the tasks that use the TCP/IP stack if they have not already
        been created. */
        if( xTasksAlreadyCreated == pdFALSE ){
            /*
             * For convenience, tasks that use FreeRTOS+TCP can be created here
             * to ensure they are not created before the network is usable.
             */

            xTasksAlreadyCreated = pdTRUE;
            DEBUGOUT("Conexão estabelecida\n");

            FreeRTOS_GetAddressConfiguration( &ulIPAddress,
                                                      &ulNetMask,
                                                      &ulGatewayAddress,
                                                      &ulDNSServerAddress );
            FreeRTOS_inet_ntoa( ulIPAddress, cBuffer );
            DEBUGOUT( "IP Address: %s\r\n", cBuffer );

            /* Convert the net mask to a string then print it out. */
            FreeRTOS_inet_ntoa( ulNetMask, cBuffer );
            DEBUGOUT( "Subnet Mask: %s\r\n", cBuffer );

            /* Convert the IP address of the gateway to a string then print it out. */
            FreeRTOS_inet_ntoa( ulGatewayAddress, cBuffer );
            DEBUGOUT( "Gateway IP Address: %s\r\n", cBuffer );

            /* Convert the IP address of the DNS server to a string then print it out. */
            FreeRTOS_inet_ntoa( ulDNSServerAddress, cBuffer );
            DEBUGOUT( "DNS server IP Address: %s\r\n", cBuffer );

            xTaskCreate(task_IOT, "t_IOT", configMINIMAL_STACK_SIZE*16, NULL, 1, NULL );
        }
    }
    else{
    	DEBUGOUT("Queda conexão\n");
    }
}
/* Use by the pseudo random number generator. */
static UBaseType_t ulNextRand;
UBaseType_t uxRand( void )
{
const uint32_t ulMultiplier = 0x015a4e35UL, ulIncrement = 1UL;

	/* Utility function to generate a pseudo random number. */

	ulNextRand = ( ulMultiplier * ulNextRand ) + ulIncrement;
	return( ( int ) ( ulNextRand >> 16UL ) & 0x7fffUL );
}
#if( ipconfigUSE_LLMNR != 0 ) || ( ipconfigUSE_NBNS != 0 ) || ( ipconfigDHCP_REGISTER_HOSTNAME == 1 )
	const char *pcApplicationHostnameHook( void )
	{
		/* Assign the name "FreeRTOS" to this network node.  This function will
		be called during the DHCP: the machine will be registered with an IP
		address plus this name. */
		return mainHOST_NAME;
	}
#endif
#if( ipconfigUSE_LLMNR != 0 ) || ( ipconfigUSE_NBNS != 0 )
	BaseType_t xApplicationDNSQueryHook( const char *pcName )
	{
	BaseType_t xReturn;

		/* Determine if a name lookup is for this node.  Two names are given
		to this node: that returned by pcApplicationHostnameHook() and that set
		by mainDEVICE_NICK_NAME. */
		if( strcmp( pcName, pcApplicationHostnameHook() ) == 0 )
		{
			xReturn = pdPASS;
		}
		else if( strcmp( pcName, mainDEVICE_NICK_NAME ) == 0 )
		{
			xReturn = pdPASS;
		}
		else
		{
			xReturn = pdFAIL;
		}

		return xReturn;
	}
#endif
uint32_t ulApplicationGetNextSequenceNumber( uint32_t ulSourceAddress,
													uint16_t usSourcePort,
													uint32_t ulDestinationAddress,
													uint16_t usDestinationPort )
{
	( void ) ulSourceAddress;
	( void ) usSourcePort;
	( void ) ulDestinationAddress;
	( void ) usDestinationPort;

	return uxRand();
}



void vLoggingPrintf( const char *pcFormatString, ... ){
	va_list ap;
	char msg[128];
	int len;

	va_start(ap, pcFormatString);
	len = vsnprintf(msg, sizeof(msg), pcFormatString, ap);
	DEBUGOUT(msg);
	//UDP_SEND_DATA(msg, len);
	va_end(ap);
	//return len;
}



void MainTask(void) {

//	debug_frmwrk_init();
	//_DBG("FreeRTOS EMWIN FatFS  Example runing\n\r");

	SystemCoreClockUpdate();
	Board_Init();

	BoardInit();

	vPortDefineHeapRegions(xHeapRegions);

	FreeRTOS_IPInit( ucIPAddress, ucNetMask, ucGatewayAddress, ucDNSServerAddress, ucMACAddress );


//	xTaskCreate(task_IOT, 		(signed char *) "t_IOT", configMINIMAL_STACK_SIZE*16, NULL, 1, NULL );
	xTaskCreate(task_MainGUI, 	(signed char *) "t_MainGUI", configMINIMAL_STACK_SIZE*16, NULL, 2, NULL );
	xTaskCreate(task_Ms, 		(signed char *) "t_Ms", configMINIMAL_STACK_SIZE, NULL, 11, NULL );
	xTaskCreate(task_Led01, 	(signed char *) "t_Led01", configMINIMAL_STACK_SIZE, NULL, 10, hdl_IOT );
	#ifdef USE_USB
//		xTaskCreate(task_USB, 		"t_USB", configMINIMAL_STACK_SIZE*20, NULL, 4, NULL );
	#endif
//	xTaskCreate(task_CTRLSys, 	(signed char *) "t_CTRLSys", configMINIMAL_STACK_SIZE*10, NULL, 5, NULL );
//	xTaskCreate(task_IOT, 		(signed char *) "t_IOT", configMINIMAL_STACK_SIZE*20, NULL, 1, NULL );


	vTaskStartScheduler();

	for(;;);
}

int main(void)
{
MainTask();
}


GUI_PID_STATE touch;

static void task_MainGUI( void *pvParameters ){
	char text[200];

	GUI_Init();
	WM_SetDesktopColor(GUI_BLACK);

	GUI_UC_SetEncodeUTF8();
	GUI_UC_SetEncodeUTF8();
	GUI_UC_SetEncodeUTF8();

	GUI_Clear();
	GUI_SetBkColor(GUI_BLACK);

	Chip_GPIO_SetPinState(LPC_GPIO, LCD[19].PORT, LCD[19].PIN, 1);
	Chip_GPIO_SetPinState(LPC_GPIO, LCD[20].PORT, LCD[20].PIN, 1);

	fs_sd = malloc(sizeof (FATFS));
	if(!f_mount(fs_sd,"0:", 1)){
		_f_SD_Connet = 1;
	}

	vTaskDelay(1000);


	DEBUGOUT("MainGUI running...\n\r");
	while (1) {

		GUI_Exec();


		switch(_f_STATUS){
			//THIS CODE WAS REMOVED
			default:{
				if(ScrLoad || UpdateScr){_print_Tela_Em_Desenvolvimento();}
			}break;
		}


//		snprintf(text,sizeof(text),"%d %d %d %d %d %d %d %d %d",IOT_MANAGER._c_time_get_status, IOT_TIME_ERROR, IOT_SENDED, IOT_ERROR, IOT_MANAGER.IOT_FRAME, IOT_LINE_PASS,IOT_Config.server.VERSAO_SERVER, IOT_Config.server.VERSAO_LOCAL,_c_ETH_Tempo_medio_resposta);
//		MensagemDebug(0,0,400,30,text);
		_c_tsk_Gui++;
		vTaskDelay(10);
	}
}

static void task_Led01( void *pvParameters ){
	uint8_t led_state=0;

	//_DBG("Led01 running...\n\r");
	while(1){
		_f_WatchDogFeed |= 0x08;
		GPIO_Set(26,led_state);
		led_state = !led_state;

		_c_tsk_Led01++;
		vTaskDelay(300);
	}
}

static void task_USB( void *pvParameters ){
	USB_Init(FlashDisk_MS_Interface.Config.PortNumber, USB_MODE_Host);
	fs_usb = malloc(sizeof (FATFS));

	while(1){

		_f_WatchDogFeed |= 0x04;
//
//		if(_f_USB_file_calib && (_f_STATUS==ST_MENU_PRINCIPAL || _f_STATUS==ST_PRE_AQUECIMENTO || _f_STATUS==ST_SEL_PRE_AQUECIMENTO)){_f_USB_Connet=0;}
//		else if(_f_USB_Connet){//PROCESSO NECESSARIO DO USB
//
//			if(!(f_mount(fs_usb, "1:", 1))){

//				switch(_f_STATUS){
//
//					case ST_USB_EXPORTAR_TENSAO:{
//						_f_USB_Process=1;
//						_f_tick_error = 0;
//						_f_tick_error = Export_Params_Tensao();
//						if(_f_tick_error){
//							STATUS = ST_USB_EXPORTAR_TENSAO_FALHA;
//						}
//						else{
//							vTaskDelay(1000);
//							STATUS= ST_CONFIGURACOES;
//						}
//					}break;
//					case ST_USB_IMPORTAR_TENSAO:{
//						_f_USB_Process=1;
//						_f_tick_error = 0;
//						_f_tick_error = Import_Params_Tensao();
//						if(_f_tick_error){
//							STATUS = ST_USB_IMPORTAR_TENSAO_FALHA;
//						}
//						else{
//							read_pwm_csv();
//							vTaskDelay(1000);
//							STATUS= ST_CONFIGURACOES;
//						}
//					}break;
//
//					case ST_CONF_EMBTECH:{
//						_f_USB_Process=1;
//						Teste_USB();
//
//					}break;
//
//					case ST_PRE_AQUECIMENTO:
//					case ST_SEL_PRE_AQUECIMENTO:
//					case ST_MENU_PRINCIPAL:{
//							_f_USB_Process=1;
//							_f_USB_file_calib = 1;
//							if(!Calib_Touch_USB()){
//								_c_TimeOFFPower = 5;
//								while(_c_TimeOFFPower){vTaskDelay(10);}
//								NVIC_SystemReset();
//							}
//					}break;
//
//					case ST_USB_UPDATE_FIRMWARE:{
//						_f_USB_Process=1;
//						if(Update_Firmware()){
//							STATUS = ST_USB_UPDATE_FIRMWARE_ERRO;
//						}
//					}break;
//
//					case ST_USB_IMPORTAR_TUDO:{
//						_f_USB_Process=1;
//						Delete_Receitas_SD();
//						if(Import_Receitas()){
//							STATUS= ST_USB_IMPORTAR_FALHA;
//						}
//						else{
//							if(Import_Config_User()){
//								STATUS= ST_USB_IMPORTAR_FALHA;
//							}
//							else{
//								STATUS= ST_CONFIGURACOES;
//							}
//						}
//					}break;
//
//					case ST_USB_EXPORTAR_TUDO:{
//						_f_USB_Process=1;
//						Delete_Receitas_USB();
//						if(Export_Receitas()){
//							STATUS= ST_USB_EXPORTAR_FALHA;
//						}
//						else{
//							if(Export_Config()){
//								STATUS= ST_USB_EXPORTAR_FALHA;
//							}
//							else{
//								STATUS= ST_CONFIGURACOES;
//							}
//						}
//					}break;
//
//					case ST_USB_EXPORTAR_RECEITAS:
//					case ST_USB_EXPORTAR_GRUPOS:{
//						_f_USB_Process=1;
//						Delete_Receitas_USB();
//						if(Export_Receitas()){
//							STATUS= ST_USB_EXPORTAR_FALHA;
//						}
//						else{
//							STATUS= ST_CONFIGURACOES;
//						}
//					}break;
//
//					case ST_USB_IMPORTAR_RECEITAS:
//					case ST_USB_IMPORTAR_GRUPOS:{
//						_f_USB_Process=1;
//						Delete_Receitas_SD();
//						if(Import_Receitas()){
//							STATUS= ST_USB_IMPORTAR_FALHA;
//						}
//						else{
//							STATUS= ST_CONFIGURACOES;
//						}
//					}break;
//					case ST_USB_IMPORTAR_CONF_USER:{
//						_f_USB_Process=1;
//						if(Import_Config_User()){
//							STATUS= ST_USB_IMPORTAR_FALHA;
//						}
//						else{
//							STATUS= ST_CONFIGURACOES;
//						}
//					}break;
//					case ST_USB_EXPORTAR_CONF_USER:{
//						_f_USB_Process=1;
//						if(Export_Config()){
//							STATUS= ST_USB_EXPORTAR_FALHA;
//						}
//						else{
//							STATUS= ST_CONFIGURACOES;
//						}
//					}break;
//				}
//
//				f_mount(0, "1:", 1);
//
//				_f_USB_Connet = 0;
//				_f_USB_Process = 0;
//			}
//		}
//		else if(_f_USB_Process){
////			STATUS = ST_USB_REMOVIDO;
//			_f_USB_Process = 0;
//		}

//		switch(_f_STATUS){
//			case ST_CONF_RESTAURAR_FABRICA_PROCESS:{
//				_f_USB_Process=1;
//				_load_padrao_fabrica();
//				_f_USB_Process=0;
//			}break;
//		}
//
//		_c_timeOutStatus++;
//		cont_USB++;
		vTaskDelay(10);
	}
}

static void task_Ms( void *pvParameters ){
	while(1){
		timerCntms++;

		_c_tsk_Ms++;
		vTaskDelay(1);
	}
}

static void task_IOT(void *pvParameters){
//	//https://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_TCP/TCP_Networking_Tutorial_Sending_TCP_Data.html
//	//https://www.freertos.org/https/preconfiguredexamples.html
//	//https://www.freertos.org/FreeRTOS_Support_Forum_Archive/July_2015/freertos_Starting_point_for_doing_GET_and_POST_Request_513904dfj.html
//	//https://www.freertos.org/FreeRTOS-Plus/FreeRTOS_Plus_UDP/API/inet_addr.html

	Socket_t xSocketSend;
	struct freertos_sockaddr xRemoteAddress;
	BaseType_t xAlreadyTransmitted = 0, xBytesSent = 0;
	//char *pcBufferToTransmit = "Hello World FreeRTOS-TCP LPC1788";
	size_t xTotalLengthToSend = 0;//strlen(pcBufferToTransmit);
	size_t xLenToSend;
	BaseType_t retCon = 0;
	char cBuffer[ 16 ];
	char buf[1024];
	char TxBuf[1024];
	char json_body[1024];

	DEBUGOUT("task_IOT start\n");
	xRemoteAddress.sin_addr = 0;

	while(1){
		if(xRemoteAddress.sin_addr != 0){
			FreeRTOS_inet_ntoa( xRemoteAddress.sin_addr, ( char * ) cBuffer );
			DEBUGOUT("IP address %s\r\n", cBuffer);
		}
		else{
			xRemoteAddress.sin_port = FreeRTOS_htons( 80 );
			xRemoteAddress.sin_addr = FreeRTOS_gethostbyname("api.tago.io");
			DEBUGOUT("error gethot\n");
		}


		if(FreeRTOS_IsNetworkUp()){

			PLINE();
			xSocketSend = FreeRTOS_socket(FREERTOS_AF_INET, FREERTOS_SOCK_STREAM, FREERTOS_IPPROTO_TCP);
			PLINE();
			configASSERT( xSocketSend != FREERTOS_INVALID_SOCKET );
			PLINE();

			//vTaskDelay(2000);

			retCon = FreeRTOS_connect( xSocketSend, &xRemoteAddress, sizeof( xRemoteAddress ) );

			if( retCon == 0 ){
				snprintf(json_body,sizeof(json_body),
						"[{\"variable\":\"cam\",\"value\":56}]");


				snprintf(TxBuf, sizeof(TxBuf),
						"POST /data HTTP/1.1\r\n"
						"Host: api.tago.io\r\n"
						"content-type: application/json\r\n"
						"content-length: %d\r\n"
						"device-token: c7f71928-9510-4cda-9c69-29a0e35d44b9\r\n\r\n"
						"%s",strlen(json_body),json_body);

				xTotalLengthToSend = strlen(TxBuf);
				DEBUGOUT("SENDING ... \n");
				DEBUGOUT("%s",TxBuf);
				xAlreadyTransmitted = 0;
				/* How many bytes are left to send? */
				xLenToSend = xTotalLengthToSend;// – xAlreadyTransmitted;
				xBytesSent = FreeRTOS_send( /* The socket being sent to. */
						xSocketSend,
						/* The data being sent. */
						&( TxBuf[ xAlreadyTransmitted ] ),
						/* The remaining length of data to send. */
						xLenToSend,
						/* ulFlags. */
						0 );

				if( xBytesSent >= 0 )
				{
					PLINE();
					/* Data was sent successfully. */
					xAlreadyTransmitted += xBytesSent;
				}
				else
				{
					PLINE();
					/* Error – break out of the loop for graceful socket close. */
				}

				PLINE();
				FreeRTOS_shutdown( xSocketSend, FREERTOS_SHUT_RDWR );
				PLINE();
				memset(&buf,0x00,sizeof(buf));
				while( FreeRTOS_recv( xSocketSend, buf, sizeof(buf), 0 ) >= 0 ){
					PLINE();
					//vTaskDelay(pdMS_TO_TICKS( 250 ) );
					DEBUGOUT("recv: %s\n",buf);
				}
			}
			else{
				DEBUGOUT("error send: %d \n", retCon);
			}

			PLINE();
			/* The socket has shut down and is safe to close. */
			FreeRTOS_closesocket( xSocketSend );
			DEBUGOUT("close socket\n");
		}

		vTaskDelay(5000);
	}

	DEBUGOUT("task_IOT stop\n");
	vTaskDelete(hdl_IOT);
}

