#ifndef _CALIBRATE_H_
#define _CALIBRATE_H_

#include <math.h>
#include "GUI.h"

void TS_Calc_Calibration(void);
void TS_Calc_New_Point_XY(int AD_X, int AD_Y, int *NewX, int *NewY);
unsigned char TS_Init_Touch(void);

void _print_calibration(unsigned char Step, unsigned char Language, const GUI_FONT GUI_UNI_PTR * pNewFont);

void TS_DrawButton(int x, int y, int w, int h, char * text,U32 color, U32 color_font, U32 color_cont, const GUI_FONT GUI_UNI_PTR * pNewFont, U8 Align, U8 Mode);
void TS_DrawRectText(int x, int y, int w, int h, char * text, U32 color_font, const GUI_FONT GUI_UNI_PTR * pNewFont,U8 Align);

#endif
