/***************************************************************************************
	Versao: 001 22/08/2018
	Autor: Ulysses C. Fonseca
	Empresa: Embtech Tecnologia Embarcada SA

	Driver de comunicacao com o sensor de temperatura na placa MCP9803

****************************************************************************************/

#ifndef __MCP9803__
#define __MCP9803__

#include "stdint.h"


#define MCP9803_ADDRESS 0x90


#define MCP9803_REG_TEMP	0x00

/*
 *	7 - ONE-SHOT
 *		1-ENABLE 0-DISABLED
 *
 *	5-6 - ADC RESOLUTION
 *		00 - 9 bit
 *		01 - 10 bit
 *		10 - 11 bit
 *		11 - 12 bit
 *
 *	3-4	- FAULT QUEUE
 *		00 - 1
 *		01 - 2
 *		10 - 4
 *		11 - 6
 *
 *	2 - ALERT POLARITY
 *		1-ACTIVE-HIGH 0-ACTIVE-LOW
 *
 *	1 - COMP/INT
 *		1-INTERRUPT MODE	0-COMPARATOR MODE
 *
 *	0 - SHUTDOWN
 *		1-ENABLE 0-DISABLE
 */
#define MCP9803_REG_CONFIG	0x01


uint16_t MCP9803_Read_register (uint8_t adress, uint8_t pointer);
void MCP9803_Write_register(uint8_t adress, uint8_t pointer, uint8_t data);



#endif
