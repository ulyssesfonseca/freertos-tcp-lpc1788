#ifndef __BOARD_TYPES_H__
#define __BOARD_TYPES_H__
#include "stdint.h"

typedef struct{
	uint8_t FUNC;
	uint8_t *FUNCs;
	uint8_t PORT;
	uint8_t PIN;
}PIN_MAP_TypeDef;

typedef struct{
	uint8_t FUNC;
	uint8_t PORT;
	uint8_t PIN;
}PIN_COM_TypeDef;

typedef struct{
	uint8_t PORT;
	uint8_t PIN;
}PIN_DEDI_TypeDef;

#endif
